// có thể lấy theo querySelector hoặc querySelectorAll

var pTag = document.querySelector(".title p");
console.log('pTag: ', pTag);
// thêm thuộc tính vào thẻ 
pTag.style.background = "red";

// Lấy tất cả thẻ có class là title
var listPTag = document.querySelectorAll(".title");
console.log('listPTag: ', listPTag);
console.log('Số lượng: ', listPTag.length);
//cho 1 phần tử trog list đó có background màu xanh biển
listPTag[1].style.background = "blue";

