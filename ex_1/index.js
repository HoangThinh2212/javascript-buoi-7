// Tạo ra chuỗi dùng cho cả bài
var numberArr = [];

function themSo() {
  var number = document.querySelector("#txt-number").value * 1;
  console.log("number: ", number);
  numberArr.push(number);
  console.log("numberArr: ", numberArr);

  var tongSoChan = 0;
  var soLuongSoAm = 0;
  var tongSoAm = 0;

  for (var index = 0; index < numberArr.length; index++) {
    var currentNumber = numberArr[index];
    if (currentNumber % 2 == 0) {
      tongSoChan = tongSoChan + currentNumber;
    }
    if (currentNumber < 0) {
      soLuongSoAm++;
      tongSoAm = tongSoAm + currentNumber;
    }
  }
  document.querySelector("#txt-number").value = "";

  document.querySelector(
    "#result"
  ).innerHTML = `<h5>Array hiện tại: ${numberArr}</h5> <p>Tổng số chẵn: ${tongSoChan}</p> <p>Số lượng số âm: ${soLuongSoAm}</p> <p>Tổng số âm: ${tongSoAm}</p>`;
}
