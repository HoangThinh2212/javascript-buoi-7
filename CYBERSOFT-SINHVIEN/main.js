var danhSachTheTr = document.querySelectorAll("#tblBody tr");
console.log("danhSachTheTr: ", danhSachTheTr);

//
var layDiemTuTheTr = function (trTag) {
  var danhSachTd = trTag.querySelectorAll("td");
  return danhSachTd[3].innerHTML * 1;
};

var layTenTuTheTr = function (trTag) {
  var danhSachTd = trTag.querySelectorAll("td");
  return danhSachTd[2].innerHTML;
};
// Tìm sinh viên giỏi nhất
var theTrLonNhat = danhSachTheTr[0];
// Tìm sinh viên yếu nhất
var theTrNhoNhat = danhSachTheTr[0];
// Tìm số sinh viên giỏi
var soSinhVienGioi = 0;
// Tìm số sinh viên >5
var danhSachSinhVienLonHon5 = "";

for (var index = 0; index < danhSachTheTr.length; index++) {
  //theTrHienTai là thẻ Tr hiện tại
  var theTrHienTai = danhSachTheTr[index];
  // Điểm trong thẻ tr hiện tại
  var diemHienTai = layDiemTuTheTr(theTrHienTai);
  // Điểm trong thẻ tr lớn nhất
  var diemLonNhat = layDiemTuTheTr(theTrLonNhat);
  var diemNhoNhat = layDiemTuTheTr(theTrNhoNhat);

  // So sánh
  if (diemHienTai > diemLonNhat) {
    theTrLonNhat = theTrHienTai;
  }
  if (diemHienTai < diemNhoNhat) {
    theTrNhoNhat = theTrHienTai;
  }
  // Tìm số sinh viên giỏi
  if (diemHienTai >= 8) {
    soSinhVienGioi++;
  }
  if (diemHienTai >= 5) {
    var contentHTML = `<p class="text-danger"> ${layTenTuTheTr(theTrHienTai)} - ${layDiemTuTheTr(
      theTrHienTai
    )} </p>`;
    danhSachSinhVienLonHon5 += contentHTML;
  }
}
// Sinh viên Giỏi nhất
document.getElementById("svGioiNhat").innerHTML = `${layTenTuTheTr(
  theTrLonNhat
)} - ${layDiemTuTheTr(theTrLonNhat)}`;
// Sinh viên yếu nhất
document.getElementById("svYeuNhat").innerHTML = `${layTenTuTheTr(
  theTrNhoNhat
)} - ${layDiemTuTheTr(theTrNhoNhat)}`;
//Số lượng sinh viên giỏi
document.getElementById("soSVGioi").innerHTML = soSinhVienGioi;
// Số lượng sinh viên > 5
document.getElementById("dsDiemHon5").innerHTML = danhSachSinhVienLonHon5;
