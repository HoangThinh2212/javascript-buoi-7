// primitive type ~ string, number, boolean

// reference type ~ array, object

var menu = ["Bánh canh", "Bún bò", "Hủ tíu"];
console.log("menu: ", menu);

// lấy số lượng hiện tại
var soLuong = menu.length;
console.log("soLuong: ", soLuong);

// thêm phần tử vào mảng
menu.push("Phở khô Gia Lai");
menu.push("Bún chả Hà Nội");
menu.push("Mỳ Quảng");
console.log("menu sau khi push: ", menu);

// Duyệt mảng
for (var index = 0; index < menu.length; index++) {
  var monAnHienTai = menu[index];

  //menu[index] , index là vị trí của mỗi phần tử trong các lần lặp
  console.log("monAnHienTai: ", monAnHienTai);

  // update giá trị của 1 phần tử trong array
  menu[0] = "Bánh bèo";
  console.log("menu sau khi update bánh bèo cho bánh canh: ", menu);
}

// forEach ~~ Duyệt mảng
var hienThiThongTin = function (monAn, index) {
  console.log("monAn - forEach: ", monAn, index);
};
menu.forEach(hienThiThongTin);

//pop
console.log("Menu trước khi pop: ", menu);
menu.pop();
console.log("Menu sau khi pop: ", menu);

// indexOf ~~ trả về giá trị của phần tử trong mảng, nếu trả về -1 là không tìm thấy
var thongTinInput = "Bún bò";
var indexThongTin = menu.indexOf(thongTinInput);
console.log("indexThongTin: ", indexThongTin);

if (indexThongTin !== -1) {
  menu[indexThongTin] = "Bánh bao";
  console.log("menu sau khi update indexThongTin: ", menu);
}

//slice splice
console.log("menu: ", menu);

// vị trí, số lượng

let indexBanhbao = menu.indexOf("Bánh bao");
menu.splice(indexBanhbao, 1);

// map ~ Thực hiện 1 function dựa trên 1 mảng cho trước và trả về 1 mảng mới
var thucDon = menu.map(function (monAn) {
  return "món " + monAn;
});
console.log("menu: ", menu);
console.log("thucDon: ", thucDon);

var scores = [10, 10, 10, 1, 1, 1, 6, 7, 9, 9, 10, 10];

var resultScores = scores.filter(function (number) {
  return number > 5; //number == 1 hoặc number == 10 hoặc true ~ sẽ trả hết cả các giá trị trong mảng scores (TEST CHO HIỂU)
});
console.log("resultScores: ", resultScores);
